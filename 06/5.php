<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <style>
            ul{
                list-style: none;
            }
            li{
                height: 30px;
                border: 1px solid black;
                width: 100px;
                background: #a3bfa8;
                display: flex;
                items-align:center;
                justify-content: center;
                margin-bottom: 10px;
                border-radius: 5px;
            }
            .dia{
                color: white
            }
        </style>
        <title>Dias de la semana</title>
    </head>
    <body>
        <h1>Dias de la semana</h1>
        <?php
        $dia=["domingo","lunes","martes","miercoles","jueves","viernes","sabado"]
        ?>
        <ul>
            <?php 
                foreach ($dia as $k =>$v){
                    echo '<li><a href="" class="dia">' .  $v . '</a></li>';
                    }
                ?>
        </ul>
    </body>
</html>
